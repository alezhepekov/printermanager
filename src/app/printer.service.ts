import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {
  APP_CONFIG,
  AppConfig,
  APP_DI_CONFIG } from './app.config';

import { Printer } from './printer';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': ''
  })
};

@Injectable({
  providedIn: 'root'
})
export class PrinterService {

  constructor(    
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient) {   
  }  

  getPrinters(): Promise<Printer[]> {
    const url = this.config.apiEndpoint + 'api/printer';
    return this.http.get<Printer[]>(url)
      .toPromise();
  }

  getPrinter(id: number): Promise<Printer> {
    const url = this.config.apiEndpoint + 'api/printer/' + id;
    return this.http.get<Printer>(url)
      .toPromise();
  }  
 
  addPrinter(printer: Printer): Promise<Printer> {
    const url = this.config.apiEndpoint + 'api/printer';
    return this.http.post<Printer>(url, printer, httpOptions)
      .toPromise();
  }

  updatePrinter(printer: Printer): Promise<Printer> {
    const url = this.config.apiEndpoint + 'api/printer';
    return this.http.put<Printer>(url, printer, httpOptions)
      .toPromise();
  }

  deletePrinter(id: number): Promise<{}> {
    const url = this.config.apiEndpoint + 'api/printer/' + id;
    return this.http.delete(url, httpOptions)
      .toPromise();
  }
}
