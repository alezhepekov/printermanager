import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { APP_CONFIG, APP_DI_CONFIG } from './app.config';

import { AppComponent } from './app.component';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxinput';
import { jqxButtonComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbuttons';

import { PrintersComponent } from './printers/printers.component';

import { PrinterService } from './printer.service';
import { AdapterService } from './adapter.service';
import { HelperService } from './helper.service';

@NgModule({
  declarations: [
    AppComponent,
    PrintersComponent,
    jqxGridComponent,
    jqxWindowComponent,
    jqxDropDownListComponent,
    jqxInputComponent,
    jqxButtonComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    { provide: APP_CONFIG, useValue: APP_DI_CONFIG },
    PrinterService,
    AdapterService,
    HelperService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
