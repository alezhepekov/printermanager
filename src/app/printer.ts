export class Printer {

  constructor() {
    this.id = null;
    this.sn = null;
    this.model_id = null;
    this.cur_paper = null;
    this.cur_mode = null;
    this.network_id = null;
    this.place_name = null;
    this.live_dt = null;
  }

  id: number;
  sn: string;
  model_id: number;
  cur_paper: string; 
  cur_mode: string;  
  network_id: number;
  place_name: string;
  live_dt: string;

  isValid(): boolean {
    return this.sn != null && this.model_id != null;
  }
}
