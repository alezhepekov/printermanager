import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {
  APP_CONFIG,
  AppConfig,
  APP_DI_CONFIG } from './app.config';

import { Adapter } from './adapter';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
    'Authorization': ''
  })
};

@Injectable({
  providedIn: 'root'
})
export class AdapterService {

  constructor(    
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient) {  
  } 

  getAdapters(printerId: number): Promise<Adapter[]> {
    const url = this.config.apiEndpoint + 'api/printer/' + printerId + '/adapters';
    return this.http.get<Adapter[]>(url)
      .toPromise();
  }

  getAdapter(printerId: number, mac: string): Promise<Adapter> {
    const url = this.config.apiEndpoint + 'api/printer/' + printerId + '/adapters/' + encodeURIComponent(mac);
    return this.http.get<Adapter>(url)
      .toPromise();
  }  
 
  addAdapter(printerId: number, adapter: Adapter): Promise<Adapter> {
    const url = this.config.apiEndpoint + 'api/printer/' + printerId + '/adapters';
    return this.http.post<Adapter>(url, adapter, httpOptions)
      .toPromise();
  }
  
  updateAdapter(printerId: number, adapter: Adapter): Promise<Adapter> {
    const url = this.config.apiEndpoint + 'api/printer/' + printerId + '/adapters';
    return this.http.put<Adapter>(url, adapter, httpOptions)
      .toPromise();
  }

  deleteAdapter(printerId: number, mac: string): Promise<{}> {
    const url = this.config.apiEndpoint + 'api/printer/' + printerId + '/adapters/' + encodeURIComponent(mac);
    return this.http.delete(url, httpOptions)
      .toPromise();
  }
}
