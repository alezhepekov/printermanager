import { Injectable, Inject } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import {
  APP_CONFIG,
  AppConfig,
  APP_DI_CONFIG } from './app.config';

@Injectable({
  providedIn: 'root'
})
export class HelperService {

  constructor(    
    @Inject(APP_CONFIG) private config: AppConfig,
    private http: HttpClient) {   
  }

  getDictionaries(): Promise<object> {
    const url = this.config.apiEndpoint + 'api/dictionaries';
    return this.http.get<object>(url)
      .toPromise();    
  }

  testPrint(mac: string): Promise<object> {    
    const url = this.config.apiEndpoint + 'api/print/initial';
    return this.http.get<object>(url, {
        params: {
          mac_address: encodeURIComponent(mac)
        }
      })
      .toPromise();      
  }
}
