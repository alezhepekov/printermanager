import { Component, ViewChild, Inject, OnInit, ElementRef } from '@angular/core';

import * as $ from 'jquery';
import * as moment from 'moment';

import alertify from 'alertifyjs/build/alertify.min.js';

import { Printer } from '../printer';
import { Adapter } from '../adapter';

import { jqxGridComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxgrid';
import { jqxWindowComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxwindow';
import { jqxDropDownListComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxdropdownlist';
import { jqxInputComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxinput';
import { jqxButtonComponent } from 'jqwidgets-scripts/jqwidgets-ts/angular_jqxbuttons';

import { PrinterService } from '../printer.service';
import { AdapterService } from '../adapter.service';
import { HelperService } from '../helper.service';

import {
  APP_CONFIG,
  AppConfig,
  APP_DI_CONFIG } from '../app.config';

@Component({
  selector: 'app-printers',
  templateUrl: './printers.component.html',
  styleUrls: ['./printers.component.css'],
  providers: [{ provide: APP_CONFIG, useValue: APP_DI_CONFIG }]
})
export class PrintersComponent implements OnInit {

  @ViewChild('printersGrid') printersGrid: jqxGridComponent;
  @ViewChild('printersFindDialog') printersFindDialog: jqxWindowComponent;
  @ViewChild('printersFindDialogDropDownList') printersFindDialogDropDownList: jqxDropDownListComponent;
  @ViewChild('printersFindDialogInput') printersFindDialogInput: jqxInputComponent;  

  printers: Printer[] = [];
  dictionaries: object = null;

  constructor(
    @Inject(APP_CONFIG) private config: AppConfig,
    private elementRef: ElementRef,
    private printerService: PrinterService,
    private adapterService: AdapterService,
    private helperService: HelperService) {    
    moment.locale('ru');   
  }

  ngOnInit() {    
    this.getDictionaries();    
  }

  getPrinters(): void {
    this.printerService.getPrinters()
      .then(printers => this.printers = printers)
      .catch(err => {
        console.error(err);
        alertify.error(err.error.error || err.error || err.message);
      });     
  }

  getDictionaries(): void {
    this.helperService.getDictionaries()
      .then(dictionaries => {
        this.dictionaries = dictionaries;

        this.modelsAdapter = this.createModelsDataAdapter();
        this.adapterTypesAdapter = this.createAdapterTypesDataAdapter();
        this.modelPaperSizesAdapter = this.createModelPaperSizesDataAdapter();
        this.modelPrintModesAdapter = this.createmodelPrintModesDataAdapter();
        this.networksAdapter = this.createNetworksTypesDataAdapter();
        this.printersGrid.source(this.createPrintersDataAdapter());        
      })
      .catch(err => {
        console.error(err);
        alertify.error(err.error.error || err.error || err.message);
      });    
  }

  getModelIdByName(name: string): number {
    let list = this.dictionaries && this.dictionaries['devices.model'] || [];
    for (let i = 0; i < list.length; i++) {
      let currentItem = list[i];
      if (currentItem.name === name) {
        return currentItem.id;
      }
    }
  }

  getAdapterTypeIdByName(name: string): number {
    let list = this.dictionaries && this.dictionaries['devices.adaptertype'] || [];
    for (let i = 0; i < list.length; i++) {
      let currentItem = list[i];
      if (currentItem.name === name) {
        return currentItem.id;
      }
    }
  }

  getNetworkIdByName(name: string): number {
    let list = this.dictionaries && this.dictionaries['networks.network'] || [];
    for (let i = 0; i < list.length; i++) {
      let currentItem = list[i];
      if (currentItem.name === name) {
        return currentItem.id;
      }
    }
  }

  dropDownSource: string[] = [
    'Серийный номер',
    'Модель',
    'Размер бумаги',
    'Тип печати',
    'Место установки'
  ];

  initPrintersButtons() {    
    let newEl = document.createElement('i');
    newEl.className = 'fas fa-plus-circle';
    let targetEl = document.querySelector('#addPrinterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let newPrinter: Printer = new Printer();
      this.printersGrid.addrow(null, newPrinter);      
    });

    newEl = document.createElement('i');
    newEl.className = 'fas fa-minus-circle';
    targetEl = document.querySelector('#deletePrinterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let printersGridSelectRowIndex = this.printersGrid.getselectedrowindex();
      let printersGridRowsCount = this.printersGrid.getdatainformation().rowscount;
      if (printersGridSelectRowIndex >= 0 && printersGridSelectRowIndex < parseFloat(printersGridRowsCount)) {
        let id = this.printersGrid.getrowid(printersGridSelectRowIndex);
        let data = this.printersGrid.getrowdata(printersGridSelectRowIndex);
        this.printersGrid.deleterow(id);        
        this.printerService.deletePrinter(data['id'])
          .then(data => {
            alertify.success('Объект был успешно удален!');
          })
          .catch(err => {
            console.error(err);
            alertify.error(err.error.error || err.error || err.message);
          });
      }
    });
    
    newEl = document.createElement('i');
    newEl.className = 'fas fa-sync-alt';
    targetEl = document.querySelector('#reloadPrinterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      this.getDictionaries();      
    });
   
    newEl = document.createElement('i');
    newEl.className = 'fas fa-search';
    targetEl = document.querySelector('#searchPrinterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {      
      this.printersFindDialog.open();
      this.printersFindDialog.move(60, 60);
    });
  }

  initAdaptersButtons() {    
    let newEl = document.createElement('i');
    newEl.className = 'fas fa-plus-circle';
    let targetEl = document.querySelector('#addAdapterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let printersGridSelectRowIndex = this.printersGrid.getselectedrowindex();
      let printersGridRowData = this.printersGrid.getrowdata(printersGridSelectRowIndex);
      let currentNestedGrid = this.nestedGridInstances[printersGridSelectRowIndex];     

      let printersGridRowsCount = this.printersGrid.getdatainformation().rowscount;
      if (printersGridSelectRowIndex >= 0 && printersGridSelectRowIndex < parseFloat(printersGridRowsCount)) {
        let newAdapter: Adapter = new Adapter();        
        currentNestedGrid.addrow(null, newAdapter);      
      }
    });

    newEl = document.createElement('i');
    newEl.className = 'fas fa-minus-circle';
    targetEl = document.querySelector('#deleteAdapterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let printersGridSelectRowIndex = this.printersGrid.getselectedrowindex();
      let printersGridRowsCount = this.printersGrid.getdatainformation().rowscount;
      if (printersGridSelectRowIndex >= 0 && printersGridSelectRowIndex < parseFloat(printersGridRowsCount)) {
        let printersGridRowData = this.printersGrid.getrowdata(printersGridSelectRowIndex);
        let currentNestedGrid = this.nestedGridInstances[printersGridSelectRowIndex];

        let currentNestedGridSelectedRowIndex = currentNestedGrid.getselectedrowindex();
        let currentNestedGridRowsCount = currentNestedGrid.getdatainformation().rowscount;
        if (currentNestedGridSelectedRowIndex >= 0 && currentNestedGridSelectedRowIndex < parseFloat(currentNestedGridRowsCount)) {
          let currentNestedGridRowId = currentNestedGrid.getrowid(currentNestedGridSelectedRowIndex);
          let currentNestedGridRowData = currentNestedGrid.getrowdata(currentNestedGridSelectedRowIndex);     

          this.adapterService.deleteAdapter(printersGridRowData['id'], currentNestedGridRowData['mac'])           
            .then(data => {
              alertify.success('Объект был успешно удален!');
            })
            .catch(err => {
              console.error(err);
              alertify.error(err.error.error || err.error || err.message);
            });

          currentNestedGrid.deleterow(currentNestedGridRowId);
        }
      }
    });
    
    newEl = document.createElement('i');
    newEl.className = 'fas fa-sync-alt';
    targetEl = document.querySelector('#reloadAdapterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {
      let printersGridSelectRowIndex = this.printersGrid.getselectedrowindex();
      let printersGridRowsCount = this.printersGrid.getdatainformation().rowscount;
      if (printersGridSelectRowIndex >= 0 && printersGridSelectRowIndex < parseFloat(printersGridRowsCount)) {
        let printersGridRowData = this.printersGrid.getrowdata(printersGridSelectRowIndex);
        let currentNestedGrid = this.nestedGridInstances[printersGridSelectRowIndex];
        let currentNestedGridDataAdapter = this.nestedGridDataAdapters[printersGridSelectRowIndex];

        currentNestedGrid.setOptions({source: currentNestedGridDataAdapter});
      }
    });
   
    newEl = document.createElement('i');
    newEl.className = 'fas fa-search';
    targetEl = document.querySelector('#searchAdapterButton');
    targetEl.appendChild(newEl);
    targetEl.addEventListener('click', (event: any): void => {    
    });

    (document.querySelector('#searchAdapterButton') as HTMLElement).style.display = 'none';    
  }

  createPrintersButtonsContainers(statusbar: any): void {    
    let buttonsContainer = document.createElement('div') ;
    buttonsContainer.style.cssText = 'overflow: hidden; position: relative; margin: 5px;';

    let addPrinterButtonContainer = document.createElement('div');
    let deletePrinterButtonContainer = document.createElement('div');
    let reloadPrinterButtonContainer = document.createElement('div');
    let searchPrinterButtonContainer = document.createElement('div');

    addPrinterButtonContainer.id = 'addPrinterButton';
    deletePrinterButtonContainer.id = 'deletePrinterButton';
    reloadPrinterButtonContainer.id = 'reloadPrinterButton';
    searchPrinterButtonContainer.id = 'searchPrinterButton';

    addPrinterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    deletePrinterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    reloadPrinterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    searchPrinterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';

    buttonsContainer.appendChild(addPrinterButtonContainer);
    buttonsContainer.appendChild(deletePrinterButtonContainer);
    buttonsContainer.appendChild(reloadPrinterButtonContainer);
    buttonsContainer.appendChild(searchPrinterButtonContainer);

    statusbar[0].appendChild(buttonsContainer);

    let addPrinterButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'      
    }
    let addPrinterButton = jqwidgets.createInstance('#addPrinterButton', 'jqxButton', addPrinterButtonOptions);

    let deletePrinterButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'
    }
    let deletePrinterButton = jqwidgets.createInstance('#deletePrinterButton', 'jqxButton', deletePrinterButtonOptions);

    let reloadPrinterButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let reloadPrinterButton = jqwidgets.createInstance('#reloadPrinterButton', 'jqxButton', reloadPrinterButtonOptions);

    let searchPrinterButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let searchPrinterButton = jqwidgets.createInstance('#searchPrinterButton', 'jqxButton', searchPrinterButtonOptions);
  }

  createAdaptersButtonsContainers(statusbar: any): void {    
    let buttonsContainer = document.createElement('div');
    buttonsContainer.style.cssText = 'overflow: hidden; position: relative; margin: 5px;';

    let addAdapterButtonContainer = document.createElement('div');
    let deleteAdapterButtonContainer = document.createElement('div');
    let reloadAdapterButtonContainer = document.createElement('div');
    let searchAdapterButtonContainer = document.createElement('div');

    addAdapterButtonContainer.id = 'addAdapterButton';
    deleteAdapterButtonContainer.id = 'deleteAdapterButton';
    reloadAdapterButtonContainer.id = 'reloadAdapterButton';
    searchAdapterButtonContainer.id = 'searchAdapterButton';

    addAdapterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    deleteAdapterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    reloadAdapterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';
    searchAdapterButtonContainer.style.cssText = 'float: left; margin-left: 5px;';

    buttonsContainer.appendChild(addAdapterButtonContainer);
    buttonsContainer.appendChild(deleteAdapterButtonContainer);
    buttonsContainer.appendChild(reloadAdapterButtonContainer);
    buttonsContainer.appendChild(searchAdapterButtonContainer);

    statusbar[0].appendChild(buttonsContainer);

    let addAdapterButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'      
    }
    let addAdapterButton = jqwidgets.createInstance('#addAdapterButton', 'jqxButton', addAdapterButtonOptions);

    let deleteAdapterButtonOptions = {
      width: 25,
      height: 25,
      value: '',      
      textPosition: 'center'
    }
    let deleteAdapterButton = jqwidgets.createInstance('#deleteAdapterButton', 'jqxButton', deleteAdapterButtonOptions);

    let reloadAdapterButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let reloadAdapterButton = jqwidgets.createInstance('#reloadAdapterButton', 'jqxButton', reloadAdapterButtonOptions);

    let searchAdapterButtonOptions = {
      width: 25,
      height: 25,
      value: '',     
      textPosition: 'center'
    }
    let searchAdapterButton = jqwidgets.createInstance('#searchAdapterButton', 'jqxButton', searchAdapterButtonOptions);
  }  

  findBtnOnClick(): void {
    this.printersGrid.clearfilters();
    let searchColumnIndex = this.printersFindDialogDropDownList.selectedIndex();
    let datafield = '';
    switch (searchColumnIndex) {
      case 0:
        datafield = 'sn';
        break;
      case 1:
        datafield = 'model';
        break;
      case 2:
        datafield = 'cur_paper';
        break;
      case 3:
        datafield = 'cur_mode';
        break;
      case 4:
        datafield = 'place_name';
        break;
    }
    let searchText = this.printersFindDialogInput.val();
    let filtergroup = new jqx.filter();
    let filter_or_operator = 1;
    let filtervalue = searchText;
    let filtercondition = 'contains';
    let filter = filtergroup.createfilter('stringfilter', filtervalue, filtercondition);
    filtergroup.addfilter(filter_or_operator, filter);
    this.printersGrid.addfilter(datafield, filtergroup);   
    this.printersGrid.applyfilters();
  }

  clearBtnOnClick(): void {
    this.printersGrid.clearfilters();
  }

  createModelsDataAdapter(): any {
    let models = this.dictionaries && this.dictionaries['devices.model'].map(x => Object.assign({}, x)) || [];

    let modelsSource: any =
    {
      datatype: 'array',
      datafields: [
        { name: 'id', type: 'number' },
        { name: 'name', type: 'string' }
      ],
      localdata: models
    };

    return new jqx.dataAdapter(modelsSource, { autoBind: true });;
  }

  modelsAdapter: any = this.createModelsDataAdapter();

  createAdapterTypesDataAdapter(): any {
    let adapterTypes = this.dictionaries && this.dictionaries['devices.adaptertype'].map(x => Object.assign({}, x)) || [];

    let adapterTypesSource: any =
    {
      datatype: 'array',
      datafields: [
        { name: 'id', type: 'number' },
        { name: 'name', type: 'string' }
      ],
      localdata: adapterTypes
    };

    return new jqx.dataAdapter(adapterTypesSource, { autoBind: true });
  }

  adapterTypesAdapter: any = this.createAdapterTypesDataAdapter();

  createModelPaperSizesDataAdapter(): any {
    let modelPaperSizes = this.dictionaries && this.dictionaries['devices.papersize'].map(x => Object.assign({}, x)) || [];

    let modelPaperSizesSource: any =
    {
      datatype: 'array',
      datafields: [
        { name: 'code', type: 'string' },
        { name: 'height', type: 'number' },
        { name: 'width', type: 'number' }
      ],
      localdata: modelPaperSizes
    };

    return new jqx.dataAdapter(modelPaperSizesSource, { autoBind: true });
  }

  modelPaperSizesAdapter: any = this.createModelPaperSizesDataAdapter();
  
  createmodelPrintModesDataAdapter(): any {
    let modelPrintModes = this.dictionaries && this.dictionaries['devices.printmode'].map(x => Object.assign({}, x)) || [];

    let modelPrintModesSource: any =
    {
      datatype: 'array',
      datafields: [       
        { name: 'code', type: 'string' }
      ],
      localdata: modelPrintModes
    };

    return new jqx.dataAdapter(modelPrintModesSource, { autoBind: true });
  }

  modelPrintModesAdapter: any = this.createmodelPrintModesDataAdapter();
 
  createNetworksTypesDataAdapter(): any {
    let networks = this.dictionaries && this.dictionaries['networks.network'].map(x => Object.assign({}, x)) || [];

    let networksSource: any =
    {
      datatype: 'array',
      datafields: [
        { name: 'id', type: 'number' },
        { name: 'name', type: 'string' }
      ],
      localdata: networks
    };

    return new jqx.dataAdapter(networksSource, { autoBind: true });
  }

  networksAdapter: any = this.createNetworksTypesDataAdapter(); 

  printersLiveDTRenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
    if (!value) {
      return '';
    }

    let nowts = moment();    
    let endts = moment(value);
    let duration = moment.duration(nowts.diff(endts));
    let hours = duration.asHours();

    let formattedValue = moment(value).format('DD.MM.YYYY HH:mm:ss');

    if (hours > 24) {
      return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: red;">' + formattedValue + '</span>';
    }
    else if (hours > 3) {
      return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + '; color: orange;">' + formattedValue + '</span>';
    }
    else {
      return '<span style="margin: 4px; float: ' + columnproperties.cellsalign + ';">' + formattedValue + '</span>';
    }
  }; 

  printersColumns: any[] =
  [
    { text: 'Серийный номер', datafield: 'sn', width: 125 },
    { text: 'Модель', datafield: 'model', cellsalign: 'right', align: 'right', columntype: 'dropdownlist',   
      createeditor: (row: number, column: any, editor: any): void => {             
        editor.jqxDropDownList({ source: this.modelsAdapter, displayMember: 'name', valueMember: 'id' });
      },
      cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
        if (newvalue == '') return oldvalue;
      }
    },
    { text: 'Размер бумаги', datafield: 'cur_paper_code', align: 'right', cellsalign: 'right', columntype: 'dropdownlist',   
      createeditor: (row: number, column: any, editor: any): void => {      
        editor.jqxDropDownList({ source: this.modelPaperSizesAdapter, displayMember: 'code', valueMember: 'code' });
      },
      cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
        if (newvalue == '') return oldvalue;
      }
    },
    { text: 'Тип печати', datafield: 'cur_mode_code', cellsalign: 'right', columntype: 'dropdownlist',   
      createeditor: (row: number, column: any, editor: any): void => {      
        editor.jqxDropDownList({ source: this.modelPrintModesAdapter, displayMember: 'code', valueMember: 'code' });
      },
      cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
        if (newvalue == '') return oldvalue;
      }
    },
    { text: 'Место установки', datafield: 'place_name', align: 'center' },
    { text: 'Время посл. успешной печати', datafield: 'live_dt', align: 'center', cellsrenderer: this.printersLiveDTRenderer }
  ];  

  createPrintersDataAdapter(): any {
    let printersSource: any =
    {
      datatype: 'json',
      datafields: [
        { name: 'id', type: 'int' },
        { name: 'sn', type: 'string' },
        { name: 'model_id', type: 'int' },      
        { name: 'model', value: 'model_id', values: { source: this.modelsAdapter.records, value: 'id', name: 'name' } },
        { name: 'cur_paper', type: 'string' },
        { name: 'cur_paper_code', value: 'cur_paper', values: { source: this.modelPaperSizesAdapter.records, value: 'code', name: 'code' } },
        { name: 'cur_mode', type: 'string' },
        { name: 'cur_mode_code', value: 'cur_mode', values: { source: this.modelPrintModesAdapter.records, value: 'code', name: 'code' } },
        { name: 'network_id', type: 'int' },
        { name: 'place_name', type: 'string' },
        { name: 'live_dt', type: 'string' }      
      ],
      root: '',
      record: '',
      url: this.config.apiEndpoint + 'api/printer',  
      updaterow: (rowid: number, rowdata: any): void => {
        const updatedPrinter = new Printer();        
  
        updatedPrinter.id = rowdata['id'];
        updatedPrinter.sn = rowdata['sn'];
        updatedPrinter.model_id = this.getModelIdByName(rowdata['model']) || null;
        updatedPrinter.cur_paper = rowdata['cur_paper_code'];
        updatedPrinter.cur_mode = rowdata['cur_mode'];
        updatedPrinter.network_id = rowdata['network_id'] || null;
        updatedPrinter.place_name = rowdata['place_name'];       

        if (updatedPrinter.isValid()) {
          this.printerService.updatePrinter(updatedPrinter)
            .then(printer => {
              alertify.success('Объект был успешно обновлен!');
              rowdata['id'] = printer.id;
            })
            .catch(err => {
              console.error(err);
              alertify.error(err.error.error || err.error || err.message);
            });
        }
        else {
          alertify.error('Ошибка сохранения объекта. Не заполнены все обязательные поля.');
        }        
      }
    };
  
    return new jqx.dataAdapter(printersSource); 
  }

  printersDataAdapter: any = this.createPrintersDataAdapter();

  nestedGrids: any[] = new Array();

  nestedGridInstances: any[] = new Array();

  nestedGridDataAdapters: any[] = new Array();

  testPrint = (mac: string): void => {   
    this.helperService.testPrint(mac)
      .then(data => {
        alertify.success('Успешная печать тестовой страницы!');
      })
      .catch(err => {
        console.error(err);
        alertify.error(err.error.error || err.error || err.message);
      }); 
  }

  initRowDetails = (index: number, parentElement: any, gridElement: any, record: any): void => {    
    let nestedGridContainer = parentElement.children[0];
    this.nestedGrids[index] = nestedGridContainer;  
   
    let url = this.config.apiEndpoint + 'api/printer/' + record.id + '/adapters';
    let adaptersSource = {
      datatype: 'json',
      datafields: [
        { name: 'mac', type: 'string' },
        { name: 'ip', type: 'string' },
        { name: 'type_id', type: 'int' },
        { name: 'type', value: 'type_id', values: { source: this.adapterTypesAdapter.records, value: 'id', name: 'name' } },
        { name: 'network_id', type: 'int' },
        { name: 'network', value: 'network_id', values: { source: this.networksAdapter.records, value: 'id', name: 'name' } },
      ],    
      url: url,
      updaterow: (rowid: number, rowdata: any): void => {       
        let printersGridSelectRowIndex = this.printersGrid.getselectedrowindex();
        let printersGridRowsCount = this.printersGrid.getdatainformation().rowscount;
        if (printersGridSelectRowIndex >= 0 && printersGridSelectRowIndex < parseFloat(printersGridRowsCount)) {
          let printersGridRowData = this.printersGrid.getrowdata(printersGridSelectRowIndex);
          let currentNestedGrid = this.nestedGridInstances[printersGridSelectRowIndex];

          let currentNestedGridSelectedRowIndex = currentNestedGrid.getselectedrowindex();
          let currentNestedGridRowsCount = currentNestedGrid.getdatainformation().rowscount;
          if (currentNestedGridSelectedRowIndex >= 0 && currentNestedGridSelectedRowIndex < parseFloat(currentNestedGridRowsCount)) {
            let currentNestedGridRowId = currentNestedGrid.getrowid(currentNestedGridSelectedRowIndex);
            let currentNestedGridRowData = currentNestedGrid.getrowdata(currentNestedGridSelectedRowIndex);     

            const updatedAdapter = new Adapter();  

            updatedAdapter.conn_type = rowdata['conn_type'];
            updatedAdapter.host_name = rowdata['host_name'];
            updatedAdapter.ip = rowdata['ip'];
            updatedAdapter.ip_dt = rowdata['ip_dt'];
            updatedAdapter.live_dt = rowdata['live_dt'];
            updatedAdapter.mac = rowdata['mac'];
            updatedAdapter.network_id = this.getNetworkIdByName(rowdata['network']) || null;
            updatedAdapter.type_id = this.getAdapterTypeIdByName(rowdata['type']) || null;         

            if (updatedAdapter.isValid()) {            
              this.adapterService.updateAdapter(printersGridRowData['id'], updatedAdapter)
                .then(adapter => {
                  alertify.success('Объект был успешно обновлен!');
                })
                .catch(err => {
                  console.error(err);
                  alertify.error(err.error.error || err.error || err.message);
                });
            }
            else {
              alertify.error('Ошибка сохранения объекта. Не заполнены все обязательные поля.');
            }            
          }
        }           
      }
    }

    let nestedGridAdapter = new jqx.dataAdapter(adaptersSource);

    this.nestedGridDataAdapters[index] = nestedGridAdapter;

    if (nestedGridContainer != null) {

      let testPrintRenderer = (row: number, columnfield: string, value: string | number, defaulthtml: string, columnproperties: any, rowdata: any): string => {
        setTimeout(() => {         
          [].slice.call(document.querySelectorAll('button.test-print-btn')).forEach((el) => {           
            if (el.dataset.status === 'handler_not_installed') {
              el.onclick = () => {               
                this.testPrint(el.dataset.mac);
              };

              el.dataset.status = 'handler_installed';
            }            
          });
        }, 100);

        return '<button class="test-print-btn" data-status="handler_not_installed" data-mac="' + rowdata['mac'] + '">Печать</button>';
      };

      let settings = {
          width: '100%',
          height: 200,
          editable: true,
          source: nestedGridAdapter, 
          columns: [
            { text: 'MAC-адрес', datafield: 'mac', width: 125 },
            { text: 'IP-адрес', datafield: 'ip', cellsalign: 'right', align: 'right' },
            { text: 'Тип', datafield: 'type', align: 'right', cellsalign: 'right', columntype: 'dropdownlist',   
              createeditor: (row: number, column: any, editor: any): void => {                    
                editor.jqxDropDownList({ source: this.adapterTypesAdapter, displayMember: 'name', valueMember: 'id' });       
              },
              cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
                if (newvalue == '') return oldvalue;
              }
            },
            { text: 'Сеть', datafield: 'network', cellsalign: 'right', columntype: 'dropdownlist',   
              createeditor: (row: number, column: any, editor: any): void => {                    
                editor.jqxDropDownList({ source: this.networksAdapter, displayMember: 'name', valueMember: 'id' });       
              },
              cellvaluechanging: (row: number, column: any, columntype: any, oldvalue: any, newvalue: any): any => {
                if (newvalue == '') return oldvalue;
              }
            },
            { text: 'Тестовая печать', align: 'center', cellsrenderer: testPrintRenderer }
          ],
          showstatusbar: true,
          renderstatusbar: this.createAdaptersButtonsContainers,
          ready: this.readyAdaptersGrid
      };

      let newNestedGridInstance = jqwidgets.createInstance(`#${nestedGridContainer.id}`, 'jqxGrid', settings);     
      this.nestedGridInstances[index] = newNestedGridInstance;
    }
  };

  rowdetailstemplate: any = {
    rowdetails: '<div id="nestedGrid" style="margin: 0px;"></div>',
    rowdetailsheight: 220,
    rowdetailshidden: true
  };

  readyPrintersGrid = (): void => {   
    let intervalId = setInterval(() => {
      let el = document.querySelector('#addPrinterButton');
      if (el) {
        this.initPrintersButtons();
        clearInterval(intervalId);
      }
    }, 100);
  };

  readyAdaptersGrid = (): void => {   
    let intervalId = setInterval(() => {
      let el = document.querySelector('#addAdapterButton');
      if (el) {
        this.initAdaptersButtons();
        clearInterval(intervalId);
      }
    }, 100);
  };
}
