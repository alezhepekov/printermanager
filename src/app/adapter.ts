export class Adapter {
  constructor() {
    this.conn_type = null;
    this.host_name = null;
    this.ip = null;
    this.ip_dt = null;
    this.live_dt = null;
    this.mac = null;
    this.network_id = null;
    this.type_id = null;
  } 

  conn_type: string;
  host_name: string;
  ip: string;
  ip_dt: string;
  live_dt: string;
  mac: string;
  network_id: number;
  type_id: number;

  isValid(): boolean {
    return this.mac != null && this.type_id != null;
  }
}
